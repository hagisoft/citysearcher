package eu.hagisoft.citysearcher.ui

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

abstract class BaseViewModel<VIEW : BaseView> : ViewModel(), LifecycleObserver {

    var view: VIEW? = null

    val inProgress = ObservableBoolean(false)

    val disposable = CompositeDisposable()

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    open fun onAttach() {
        //no-op
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    open fun onDetach() {
        //no-op
    }

    override fun onCleared() {
        view = null
        disposable.dispose()
    }
}