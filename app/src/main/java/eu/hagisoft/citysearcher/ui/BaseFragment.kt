package eu.hagisoft.citysearcher.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import org.jetbrains.anko.toast
import androidx.lifecycle.ViewModelProviders
import eu.hagisoft.citysearcher.BR

@SuppressLint("ValidFragment")
abstract class BaseFragment<VIEW : BaseView, VM : BaseViewModel<VIEW>, BINDING : ViewDataBinding> constructor(
    private val layoutResId: Int
) : Fragment(), BaseView {

    protected lateinit var viewModel: VM

    protected lateinit var binding: BINDING

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, layoutResId, container, false)
        return binding.root
    }

    protected inline fun <reified T : VM> setup(view: VIEW) {
        viewModel = ViewModelProviders.of(this).get(T::class.java)
        viewModel.view = view
        binding.setVariable(BR.viewModel, viewModel)
        lifecycle.addObserver(viewModel)
    }

    override fun onDestroy() {
        lifecycle.removeObserver(viewModel)
        super.onDestroy()
    }

    override fun showMessage(text: String) {
        activity?.toast(text)
    }
}
