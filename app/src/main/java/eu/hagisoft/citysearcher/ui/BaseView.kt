package eu.hagisoft.citysearcher.ui

interface BaseView {
    fun showMessage(text: String)
}