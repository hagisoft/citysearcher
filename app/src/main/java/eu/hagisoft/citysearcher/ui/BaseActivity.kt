package eu.hagisoft.citysearcher.ui

import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProviders
import eu.hagisoft.citysearcher.BR
import org.jetbrains.anko.toast

abstract class BaseActivity<VIEW : BaseView, VM : BaseViewModel<VIEW>, BINDING : ViewDataBinding> :
    AppCompatActivity(), BaseView {

    protected lateinit var viewModel: VM

    protected lateinit var binding: BINDING

    protected inline fun <reified T : VM> setup(layoutRes: Int, view: VIEW) {
        binding = DataBindingUtil.setContentView(this, layoutRes)
        viewModel = ViewModelProviders.of(this).get(T::class.java)

        viewModel.view = view
        binding.setVariable(BR.viewModel, viewModel)
        lifecycle.addObserver(viewModel)
    }

    override fun onDestroy() {
        lifecycle.removeObserver(viewModel)
        super.onDestroy()
    }

    override fun showMessage(text: String) {
        toast(text)
    }
}