package eu.hagisoft.citysearcher

import android.app.Application
import androidx.multidex.MultiDexApplication
import eu.hagisoft.citysearcher.di.AppComponent
import eu.hagisoft.citysearcher.di.AppModule
import eu.hagisoft.citysearcher.di.DaggerAppComponent

class App : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()

        initDagger()
    }

    private fun initDagger() {
        appComponent = DaggerAppComponent.builder().appModule(AppModule(this)).build()
    }

    companion object {
        lateinit var appComponent: AppComponent
    }
}