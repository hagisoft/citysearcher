package eu.hagisoft.citysearcher.repositories

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import eu.hagisoft.citysearcher.repositories.data.City

class CityDeserializer : StdDeserializer<City> {

    constructor() : this(null)

    constructor(vc: Class<*>?) : super(vc)

    override fun deserialize(p: JsonParser, ctxt: DeserializationContext): City {
        val node = p.codec.readTree<JsonNode>(p)

        val country = node.get("country").asText()
        val name = node.get("name").asText()
        val lat = node.get("coord").get("lat").asDouble()
        val lon = node.get("coord").get("lon").asDouble()

        return City(
            country = country,
            name = name,
            lat = lat,
            lon = lon
        )
    }
}