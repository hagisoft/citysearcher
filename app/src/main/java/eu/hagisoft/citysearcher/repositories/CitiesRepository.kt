package eu.hagisoft.citysearcher.repositories

import android.content.Context
import android.content.res.AssetManager
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import eu.hagisoft.citysearcher.repositories.data.City
import io.reactivex.Completable
import io.reactivex.Single

interface CitiesRepository {
    fun getAllCities(): Single<List<City>>
    fun getCitiesFilteredByName(query: String): Single<List<City>>
    fun prefetchAllCities(): Completable
}

class CitiesRepositoryImpl(val context: Context) : CitiesRepository {

    private val objectMapper = ObjectMapper().registerModule(KotlinModule())

    private var allCities = mutableListOf<City>()

    /**
     * I am splitting the list of cities into sub-lists. Each list contains cities whose name has
     * the letter which is the list's key in the map. This setup allows me to do subsequent filtering
     * on a shorter list, which improves performance
     */
    private val alphabetMapping = mapOf<Char, MutableSet<City>>(
        'a' to mutableSetOf(),
        'b' to mutableSetOf(),
        'c' to mutableSetOf(),
        'd' to mutableSetOf(),
        'e' to mutableSetOf(),
        'f' to mutableSetOf(),
        'g' to mutableSetOf(),
        'h' to mutableSetOf(),
        'i' to mutableSetOf(),
        'j' to mutableSetOf(),
        'k' to mutableSetOf(),
        'l' to mutableSetOf(),
        'm' to mutableSetOf(),
        'n' to mutableSetOf(),
        'o' to mutableSetOf(),
        'p' to mutableSetOf(),
        'q' to mutableSetOf(),
        'r' to mutableSetOf(),
        's' to mutableSetOf(),
        't' to mutableSetOf(),
        'u' to mutableSetOf(),
        'v' to mutableSetOf(),
        'w' to mutableSetOf(),
        'x' to mutableSetOf(),
        'y' to mutableSetOf(),
        'z' to mutableSetOf()
    )

    override fun prefetchAllCities(): Completable =
        Completable.fromAction {
            try {
                allCities.clear()
                val inputStream = context.assets.open("cities.json", AssetManager.ACCESS_STREAMING)
                allCities = objectMapper.readValue<List<City>>(inputStream).toMutableList()
                inputStream.close()

                allCities.sortBy { it.name }
                allCities.forEach { city ->
                    city.name.forEach {
                        alphabetMapping[it]?.add(city)
                    }
                }
                Completable.complete()
            } catch (e: Exception) {
                Completable.error(e)
            }
        }

    override fun getAllCities(): Single<List<City>> =
        if (allCities.isNullOrEmpty()) {
            prefetchAllCities().toSingle { allCities }
        } else
            Single.just(allCities)

    override fun getCitiesFilteredByName(query: String): Single<List<City>> =
        Single.fromCallable {
            val prefetchedList = alphabetMapping[query.first()]

            val results = when {
                query.length == 1 -> prefetchedList
                else -> prefetchedList?.filter { it.nameHas(query) }
            }
            results?.toList() ?: allCities.filter { it.nameHas(query) }
        }
}