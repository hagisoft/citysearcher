package eu.hagisoft.citysearcher.repositories.data

import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import eu.hagisoft.citysearcher.repositories.CityDeserializer

@JsonDeserialize(using = CityDeserializer::class)
data class City(
    val country: String,
    val name: String,
    val lat: Double,
    val lon: Double
) {
    fun nameHas(query: String) = name.contains(query, true)
}