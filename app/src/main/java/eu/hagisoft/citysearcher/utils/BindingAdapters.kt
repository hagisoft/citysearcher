package eu.hagisoft.citysearcher.utils

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableList
import androidx.recyclerview.widget.RecyclerView
import eu.hagisoft.citysearcher.features.showcities.CitiesAdapter
import eu.hagisoft.citysearcher.features.showcities.uimodels.CityUiModel

@BindingAdapter("items")
fun setImageResource(recyclerView: RecyclerView, items: ObservableArrayList<CityUiModel>) {
    recyclerView.adapter = CitiesAdapter(items)
}

@BindingAdapter("visible")
fun adjustViewVisibility(view: View, visible: Boolean) {
    view.visibility = if (visible) View.VISIBLE else View.GONE
}
