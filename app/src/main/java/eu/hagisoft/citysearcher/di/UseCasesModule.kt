package eu.hagisoft.citysearcher.di

import dagger.Module
import dagger.Provides
import eu.hagisoft.citysearcher.features.showcities.usecases.GetAllCitiesUseCase
import eu.hagisoft.citysearcher.features.showcities.usecases.GetAllCitiesUseCaseImpl
import eu.hagisoft.citysearcher.features.showcities.usecases.GetFilteredCitiesUseCase
import eu.hagisoft.citysearcher.features.showcities.usecases.GetFilteredCitiesUseCaseImpl
import eu.hagisoft.citysearcher.features.splashscreen.PrefetchAllCitiesUseCase
import eu.hagisoft.citysearcher.features.splashscreen.PrefetchAllCitiesUseCaseImpl
import eu.hagisoft.citysearcher.repositories.CitiesRepository

@Module
class UseCasesModule {

    @Provides
    fun provideGetAllCitiesUseCase(citiesRepository: CitiesRepository): GetAllCitiesUseCase =
        GetAllCitiesUseCaseImpl(citiesRepository)

    @Provides
    fun provideGetFilteredCitiesUseCase(citiesRepository: CitiesRepository): GetFilteredCitiesUseCase =
        GetFilteredCitiesUseCaseImpl(citiesRepository)

    @Provides
    fun providePrefetchAllCitiesUseCase(citiesRepository: CitiesRepository): PrefetchAllCitiesUseCase =
        PrefetchAllCitiesUseCaseImpl(citiesRepository)
}