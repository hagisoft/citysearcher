package eu.hagisoft.citysearcher.di

import android.content.Context
import dagger.Module
import dagger.Provides
import eu.hagisoft.citysearcher.repositories.CitiesRepository
import eu.hagisoft.citysearcher.repositories.CitiesRepositoryImpl
import javax.inject.Singleton

@Module
class RepositoriesModule {

    @Singleton
    @Provides
    fun provideCitiesRepository(context: Context): CitiesRepository =
        CitiesRepositoryImpl(context)
}