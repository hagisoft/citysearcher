package eu.hagisoft.citysearcher.di

import dagger.Component
import eu.hagisoft.citysearcher.App
import eu.hagisoft.citysearcher.features.MainActivityViewModel
import eu.hagisoft.citysearcher.features.map.MapViewModel
import eu.hagisoft.citysearcher.features.showcities.SearchCitiesViewModel
import eu.hagisoft.citysearcher.features.splashscreen.SplashScreenViewModel
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, RepositoriesModule::class, UseCasesModule::class])
interface AppComponent {
    fun inject(app: App)
    fun inject(mainActivityViewModel: MainActivityViewModel)
    fun inject(splashScreenViewModel: SplashScreenViewModel)
    fun inject(searchCitiesViewModel: SearchCitiesViewModel)
    fun inject(mapViewModel: MapViewModel)
}