package eu.hagisoft.citysearcher.di

import android.content.Context
import dagger.Module
import dagger.Provides
import eu.hagisoft.citysearcher.utils.StringProvider
import eu.hagisoft.citysearcher.utils.StringProviderImpl

@Module
class AppModule(private val app: Context) {

    @Provides
    fun provideContext(): Context = app

    @Provides
    fun provideStringProvider(context: Context): StringProvider = StringProviderImpl(context)
}