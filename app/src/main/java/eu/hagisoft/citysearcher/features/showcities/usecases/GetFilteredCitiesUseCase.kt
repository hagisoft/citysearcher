package eu.hagisoft.citysearcher.features.showcities.usecases

import eu.hagisoft.citysearcher.repositories.CitiesRepository
import eu.hagisoft.citysearcher.repositories.data.City
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit

interface GetFilteredCitiesUseCase {
    fun getCities(
        publishSubject: PublishSubject<String>,
        observer: DisposableObserver<List<City>>
    ): DisposableObserver<List<City>>
}

class GetFilteredCitiesUseCaseImpl(private val citiesRepository: CitiesRepository) : GetFilteredCitiesUseCase {
    override fun getCities(
        publishSubject: PublishSubject<String>,
        observer: DisposableObserver<List<City>>
    ) : DisposableObserver<List<City>> =
        publishSubject
            .debounce(300, TimeUnit.MILLISECONDS, Schedulers.computation())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .switchMapSingle {
                citiesRepository.getCitiesFilteredByName(it)
            }
            .subscribeWith(observer)
}