package eu.hagisoft.citysearcher.features.map

import android.os.Bundle
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import eu.hagisoft.citysearcher.R
import eu.hagisoft.citysearcher.databinding.MapFragmentBinding
import eu.hagisoft.citysearcher.features.MainActivity
import eu.hagisoft.citysearcher.ui.BaseFragment

class MapFragment :
    BaseFragment<MapView, MapViewModel, MapFragmentBinding>(
        R.layout.map_fragment
    ), MapView, OnMapReadyCallback {
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setup<MapViewModel>(this)

        if (checkPlayServices()) {
            binding.mapView.onCreate(null)
            binding.mapView.getMapAsync(this)
        } else {
            showMessage("Please install GooglePlay services to show the map")
            (activity as MainActivity).showSearchScreen()
        }
    }

    override fun onStart() {
        super.onStart()
        binding.mapView.onStart()
    }

    override fun onResume() {
        super.onResume()
        binding.mapView.onResume()
    }

    override fun onPause() {
        binding.mapView.onPause()
        super.onPause()
    }

    override fun onStop() {
        binding.mapView.onStop()
        super.onStop()
    }

    override fun onDestroy() {
        binding.mapView.onDestroy()
        super.onDestroy()
    }

    private fun checkPlayServices(): Boolean {
        val status =
            GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(requireActivity())
        return status == ConnectionResult.SUCCESS
    }

    override fun onMapReady(map: GoogleMap?) {
        val lat = arguments?.getDouble(LAT_KEY)
        val lon = arguments?.getDouble(LON_KEY)
        val name = arguments?.getString(NAME_KEY)

        if (lat != null && lon != null && map != null) {
            val latLng = LatLng(lat, lon)
            map.addMarker(
                MarkerOptions()
                    .position(latLng)
                    .title(name ?: "")
            )
            map.animateCamera(
                CameraUpdateFactory.newLatLngZoom(latLng, 5.0f)
            )
        }
    }

    companion object {
        const val LAT_KEY = "eu.hagisoft.citysearcher.features.map.lat_key"
        const val LON_KEY = "eu.hagisoft.citysearcher.features.map.lon_key"
        const val NAME_KEY = "eu.hagisoft.citysearcher.features.map.name_key"

        fun newInstance(lat: Double, lon: Double, name: String) = MapFragment().apply {
            arguments = Bundle().apply {
                putDouble(LAT_KEY, lat)
                putDouble(LON_KEY, lon)
                putString(NAME_KEY, name)
            }
        }
    }
}