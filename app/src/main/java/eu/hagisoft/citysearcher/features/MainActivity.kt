package eu.hagisoft.citysearcher.features

import android.os.Bundle
import androidx.fragment.app.Fragment
import eu.hagisoft.citysearcher.R
import eu.hagisoft.citysearcher.databinding.MainActivityBinding
import eu.hagisoft.citysearcher.features.map.MapFragment
import eu.hagisoft.citysearcher.features.showcities.SearchCitiesFragment
import eu.hagisoft.citysearcher.features.splashscreen.SplashScreenFragment
import eu.hagisoft.citysearcher.ui.BaseActivity

class MainActivity : BaseActivity<MainActivityView, MainActivityViewModel, MainActivityBinding>(),
    MainActivityView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setup<MainActivityViewModel>(R.layout.main_activity, this)
        showSplashScreen()
    }

    fun showSplashScreen() {
        showFragment(SplashScreenFragment.newInstance())
    }

    fun showSearchScreen() {
        showFragment(SearchCitiesFragment.newInstance())
    }

    fun showMapScreen(lat: Double, lon: Double, name: String) {
        showFragment(MapFragment.newInstance(lat, lon, name), true)
    }

    private fun showFragment(fragment: Fragment, addToBackstack: Boolean = false) {
        val transaction = supportFragmentManager
            .beginTransaction()
            .replace(R.id.main_activity_root, fragment)

        if (addToBackstack) {
            transaction.addToBackStack(fragment.javaClass.name)
        }

        transaction.commit()
    }

    fun goBack() {
        onBackPressed()
    }
}
