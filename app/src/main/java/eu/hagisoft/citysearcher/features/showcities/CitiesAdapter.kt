package eu.hagisoft.citysearcher.features.showcities

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableList
import androidx.recyclerview.widget.RecyclerView
import eu.hagisoft.citysearcher.databinding.ItemCityBinding
import eu.hagisoft.citysearcher.features.showcities.uimodels.CityUiModel

class CitiesAdapter(private val items: ObservableArrayList<CityUiModel>) : RecyclerView.Adapter<CityViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CityViewHolder {
        val cityBinding = ItemCityBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)

        return CityViewHolder(cityBinding)
    }

    override fun getItemCount(): Int = items.count()

    override fun onBindViewHolder(holder: CityViewHolder, position: Int) {
        holder.binding.item = items[position]
    }
}

class CityViewHolder(val binding: ItemCityBinding) : RecyclerView.ViewHolder(binding.root)