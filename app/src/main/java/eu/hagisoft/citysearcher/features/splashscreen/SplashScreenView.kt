package eu.hagisoft.citysearcher.features.splashscreen

import eu.hagisoft.citysearcher.ui.BaseView

interface SplashScreenView : BaseView {
    fun showMainScreen()
}