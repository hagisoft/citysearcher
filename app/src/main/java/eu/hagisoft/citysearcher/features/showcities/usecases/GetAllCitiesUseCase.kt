package eu.hagisoft.citysearcher.features.showcities.usecases

import eu.hagisoft.citysearcher.repositories.CitiesRepository
import eu.hagisoft.citysearcher.repositories.data.City
import io.reactivex.Single
import javax.inject.Inject

interface GetAllCitiesUseCase {
    fun getCities() : Single<List<City>>
}

class GetAllCitiesUseCaseImpl (private val repository: CitiesRepository) : GetAllCitiesUseCase {

    override fun getCities(): Single<List<City>> {
        return repository.getAllCities()
    }
}
