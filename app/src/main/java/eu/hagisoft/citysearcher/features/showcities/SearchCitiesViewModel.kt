package eu.hagisoft.citysearcher.features.showcities

import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import eu.hagisoft.citysearcher.App
import eu.hagisoft.citysearcher.R
import eu.hagisoft.citysearcher.features.showcities.uimodels.CityUiModel
import eu.hagisoft.citysearcher.features.showcities.usecases.GetAllCitiesUseCase
import eu.hagisoft.citysearcher.features.showcities.usecases.GetFilteredCitiesUseCase
import eu.hagisoft.citysearcher.repositories.data.City
import eu.hagisoft.citysearcher.ui.BaseViewModel
import eu.hagisoft.citysearcher.utils.StringProvider
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class SearchCitiesViewModel : BaseViewModel<SearchCitiesView>(), OnItemClickListener {
    private var searchSubject: PublishSubject<String> = PublishSubject.create()

    val cities = ObservableArrayList<CityUiModel>()

    val citiesLoadedField = ObservableField<String>()
    val citiesListVisible = ObservableBoolean(false)

    @Inject
    lateinit var getAllCitiesUseCase: GetAllCitiesUseCase

    @Inject
    lateinit var getFilteredCitiesUseCase: GetFilteredCitiesUseCase

    @Inject
    lateinit var stringProvider: StringProvider

    init {
        App.appComponent.inject(this)
        disposable.add(
            getFilteredCitiesUseCase.getCities(
                searchSubject,
                FilteredCitiesObserver()
            )
        )
    }

    override fun onAttach() {
        super.onAttach()

        if (cities.isEmpty()) {
            getAllCities()
        }
    }

    private fun getAllCities() {
        disposable.add(getAllCitiesUseCase.getCities()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { inProgress.set(true) }
            .doOnEvent { _, _ -> inProgress.set(false) }
            .subscribe(
                {
                    udpateCitiesView(it)
                },
                {
                    view?.showMessage("Error loading all cities")
                }
            )
        )
    }

    private fun udpateCitiesView(list: List<City>) {
        cities.clear()
        cities.addAll(
            list.map {
                CityUiModel.fromCity(it, this@SearchCitiesViewModel)
            }
        )
        citiesLoadedField.set(
            stringProvider.getQuantityString(
                R.plurals.loaded_cities_count,
                cities.count(),
                cities.count()
            )
        )
        citiesListVisible.set(cities.isNotEmpty())
    }

    override fun onItemClick(item: CityUiModel) {
        view?.showCityOnMap(item.lat, item.lon, item.name)
    }

    fun onNewQuery(query: String) {
        if (query.isNotBlank()) {
            inProgress.set(true)
            searchSubject.onNext(query)
        } else {
            getAllCities()
        }
    }

    inner class FilteredCitiesObserver : DisposableObserver<List<City>>() {
        override fun onComplete() {
            inProgress.set(false)
        }

        override fun onNext(list: List<City>) {
            inProgress.set(false)
            udpateCitiesView(list)
        }

        override fun onError(e: Throwable) {
            inProgress.set(false)
            view?.showMessage("Error filtering cities")
        }
    }
}

interface OnItemClickListener {
    fun onItemClick(item: CityUiModel)
}