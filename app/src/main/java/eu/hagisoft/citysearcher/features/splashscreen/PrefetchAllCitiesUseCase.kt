package eu.hagisoft.citysearcher.features.splashscreen

import eu.hagisoft.citysearcher.repositories.CitiesRepository
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

interface PrefetchAllCitiesUseCase {
    fun prefetch() : Completable
}

class PrefetchAllCitiesUseCaseImpl(private val repository: CitiesRepository) : PrefetchAllCitiesUseCase {

    override fun prefetch(): Completable =
        repository.prefetchAllCities()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
}