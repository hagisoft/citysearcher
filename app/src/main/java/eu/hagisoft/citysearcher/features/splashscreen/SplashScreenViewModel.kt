package eu.hagisoft.citysearcher.features.splashscreen

import android.util.Log
import eu.hagisoft.citysearcher.App
import eu.hagisoft.citysearcher.ui.BaseViewModel
import javax.inject.Inject

class SplashScreenViewModel : BaseViewModel<SplashScreenView>() {

    @Inject
    lateinit var prefetchAllCitiesUseCase: PrefetchAllCitiesUseCase

    init {
        App.appComponent.inject(this)
    }

    override fun onAttach() {
        super.onAttach()

        disposable.add(prefetchAllCitiesUseCase.prefetch().subscribe {
            view?.showMainScreen()
        })
    }
}