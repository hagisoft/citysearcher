package eu.hagisoft.citysearcher.features.map

import eu.hagisoft.citysearcher.App
import eu.hagisoft.citysearcher.ui.BaseViewModel

class MapViewModel : BaseViewModel<MapView>() {

    init {
        App.appComponent.inject(this)
    }
}