package eu.hagisoft.citysearcher.features.showcities.uimodels

import eu.hagisoft.citysearcher.features.showcities.OnItemClickListener
import eu.hagisoft.citysearcher.repositories.data.City

data class CityUiModel(
    val name: String,
    val country: String,
    val lat: Double,
    val lon: Double,
    val listener: OnItemClickListener
) {
    companion object {
        fun fromCity(city: City, listener: OnItemClickListener) =
            CityUiModel(
                city.name,
                city.country,
                city.lat,
                city.lon,
                listener
            )
    }
}