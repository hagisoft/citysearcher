package eu.hagisoft.citysearcher.features.showcities

import eu.hagisoft.citysearcher.ui.BaseView

interface SearchCitiesView : BaseView {
    fun showCityOnMap(lat: Double, lon: Double, name: String)
}