package eu.hagisoft.citysearcher.features.splashscreen

import android.os.Bundle
import eu.hagisoft.citysearcher.R
import eu.hagisoft.citysearcher.databinding.SplashscreenFragmentBinding
import eu.hagisoft.citysearcher.features.MainActivity
import eu.hagisoft.citysearcher.ui.BaseFragment

class SplashScreenFragment :
    BaseFragment<SplashScreenView, SplashScreenViewModel, SplashscreenFragmentBinding>(
        R.layout.splashscreen_fragment
    ), SplashScreenView {

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setup<SplashScreenViewModel>(this)
    }

    override fun showMainScreen() {
        (activity as MainActivity).showSearchScreen()
    }

    companion object {
        fun newInstance() = SplashScreenFragment()
    }
}