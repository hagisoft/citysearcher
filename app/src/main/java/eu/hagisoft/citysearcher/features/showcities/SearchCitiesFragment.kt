package eu.hagisoft.citysearcher.features.showcities

import android.app.SearchManager
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import eu.hagisoft.citysearcher.R
import eu.hagisoft.citysearcher.databinding.SearchCitiesFragmentBinding
import eu.hagisoft.citysearcher.features.MainActivity
import eu.hagisoft.citysearcher.ui.BaseFragment

class SearchCitiesFragment :
    BaseFragment<SearchCitiesView, SearchCitiesViewModel, SearchCitiesFragmentBinding>(
        R.layout.search_cities_fragment
    ), SearchCitiesView, SearchView.OnQueryTextListener {

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setup<SearchCitiesViewModel>(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.citiesSearchResultRv.layoutManager =
                LinearLayoutManager(activity, RecyclerView.VERTICAL, false)

        with(binding.citiesSearch) {
            val searchManager =
                requireActivity().getSystemService(android.content.Context.SEARCH_SERVICE) as SearchManager
            setSearchableInfo(searchManager.getSearchableInfo(requireActivity().componentName))
            setOnQueryTextListener(this@SearchCitiesFragment)
            queryHint = getString(eu.hagisoft.citysearcher.R.string.search_hint)
        }
    }

    override fun showCityOnMap(lat: Double, lon: Double, name: String) {
        (activity as MainActivity).showMapScreen(lat, lon, name)
    }

    override fun onQueryTextSubmit(query: String?): Boolean = onQueryTextChange(query)

    override fun onQueryTextChange(query: String?): Boolean {
        query?.let { viewModel.onNewQuery(it) }
        return false
    }

    companion object {
        fun newInstance() = SearchCitiesFragment()
    }
}